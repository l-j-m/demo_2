package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;

import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Fragment blankFragment=new BlankFragment();
    private Fragment blankFragment1=new BlankFragment1();
    private Fragment blankFragment2=new BlankFragment2();
    private Fragment blankFragment3=new BlankFragment3();
    private FragmentManager fragmentManager;
    private LinearLayout linearLayout, linearLayout1, linearLayout2, linearLayout3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayout=findViewById(R.id.linearlayout);
        linearLayout1=findViewById(R.id.linearlayout1);
        linearLayout2=findViewById(R.id.linearlayout2);
        linearLayout3=findViewById(R.id.linearlayout3);

        linearLayout.setOnClickListener(this);
        linearLayout1.setOnClickListener(this);
        linearLayout2.setOnClickListener(this);
        linearLayout3.setOnClickListener(this);

        initfragment();
        showfragment(0);


    }
    private void initfragment(){
        fragmentManager=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame,blankFragment);
        fragmentTransaction.add(R.id.frame,blankFragment1);
        fragmentTransaction.add(R.id.frame,blankFragment2);
        fragmentTransaction.add(R.id.frame,blankFragment3);
        fragmentTransaction.commit();
    }
    private void hideFragment(FragmentTransaction fragmentTransaction){
        fragmentTransaction.hide(blankFragment);
        fragmentTransaction.hide(blankFragment1);
        fragmentTransaction.hide(blankFragment2);
        fragmentTransaction.hide(blankFragment3);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.linearlayout:
                showfragment(0);
                break;
            case R.id.linearlayout1:
                showfragment(1);
                break;
            case R.id.linearlayout2:
                showfragment(2);
                break;
            case R.id.linearlayout3:
                showfragment(3);
                break;
            default:
                break;
        }

    }

    private void showfragment(int i) {
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        hideFragment(fragmentTransaction);
        switch (i){
            case 0:
                fragmentTransaction.show(blankFragment);
                break;
            case 1:
                fragmentTransaction.show(blankFragment1);
                break;
            case 2:
                fragmentTransaction.show(blankFragment2);
                break;
            case 3:
                fragmentTransaction.show(blankFragment3);
                break;
        }
        fragmentTransaction.commit();
    }


}
