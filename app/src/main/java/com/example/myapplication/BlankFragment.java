package com.example.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


public class BlankFragment extends Fragment {
    private View view;
    private RecyclerView recyclerView;
    private List<String> list;
    private Context context;
    private Myadapter adapterDome;

    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context = this.getActivity();
        view=inflater.inflate(R.layout.fragment_blank,container,false);
        recyclerView = view.findViewById(R.id.recycler_view);
        list = new ArrayList<>();
        for (int i=0;i<10;i++){
            list.add("这是第"+i+"个新闻");
        }
        adapterDome = new Myadapter(list,context);
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapterDome);
        adapterDome.setOnItemClickListener(new Myadapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getActivity(),NewsActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        return view;
    }

}
